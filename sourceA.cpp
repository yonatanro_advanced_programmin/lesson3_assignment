#include "sqlite3.h"
#include <fstream>
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>
using namespace std;
unordered_map<string, vector<string>> results;

int callback(void* notUsed, int argc, char** argv, char** azCol);
void printTable();
void clearTable();

void main()
{
	int rc;
	sqlite3* db;
	char *zErrMsg = 0;
	rc = sqlite3_open("FirstPart.db", &db);
	if (rc)
	{
		cout << "Can't open database";
		sqlite3_close(db);
		system("pause");
		return;
	}
	rc = sqlite3_exec(db, "CREATE TABLE people(ID INTEGER PRIMARY KEY autoincrement, name name);", callback, 0, &zErrMsg);//create the table
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return;
	}
	rc = sqlite3_exec(db, "INSERT INTO people values(NULL,'Yonatan');", callback, 0, &zErrMsg);//enter the first person
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return ;
	}
	rc = sqlite3_exec(db, "INSERT INTO people values(NULL,'Mj');", callback, 0, &zErrMsg);//enter the second person
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return ;
	}
	rc = sqlite3_exec(db, "INSERT INTO people values(NULL,'Shaq');", callback, 0, &zErrMsg);//enter the third person
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return ;
	}
	rc = sqlite3_exec(db, "UPDATE people SET name = 'some name' WHERE ID = 3;", callback, 0, &zErrMsg);//change the name of the third person
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return ;
	}
	rc = sqlite3_exec(db, "select * from people;", callback, 0, &zErrMsg);//print the table
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return ;
	}
	else
	{
		printTable();
	}
	system("pause");
	clearTable();
	rc = sqlite3_exec(db, "drop table people;", callback, 0, &zErrMsg);
}


int callback(void* notUsed, int argc, char** argv, char** azCol)
{
	int i;

	for (i = 0; i < argc; i++)
	{
		auto it = results.find(azCol[i]);
		if (it != results.end())
		{
			it->second.push_back(argv[i]);
		}
		else
		{
			pair<string, vector<string>> p;
			p.first = azCol[i];
			p.second.push_back(argv[i]);
			results.insert(p);
		}
	}

	return 0;
}

void clearTable()
{
	for (auto it = results.begin(); it != results.end(); ++it)
	{
		it->second.clear();
	}
	results.clear();
}

void printTable()
{
	auto iter = results.end();
	iter--;
	//	int size = results.begin()->second.size();
	int size = iter->second.size();
	for (int i = -2; i < size; i++)
	{
		for (auto it = results.begin(); it != results.end(); ++it)
		{
			if (i == -2)
			{
				//cout << it->first << " ";
				printf("|%*s|", 13, it->first.c_str());
			}
			else if (i == -1)
				cout << "_______________";
			else
				//	cout << it->second.at(i) << " ";
				printf("|%*s|", 13, it->second.at(i).c_str());
		}
		cout << endl;
	}
}