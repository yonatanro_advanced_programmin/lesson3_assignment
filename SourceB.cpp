#include "sqlite3.h"
#include <fstream>
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>
using namespace std;
unordered_map<string, vector<string>> results;

void whoCanBuy(int carId, sqlite3* db, char* zErrMsg);
bool balanceTransfer(int from, int to, int amount, sqlite3* db, char* zErrMsg);
bool carPurchase(int buyerid, int carid, sqlite3* db, char* zErrMsg);
int callback(void* notUsed, int argc, char** argv, char** azCol);
void printTable();
void clearTable();

void main()
{
	int rc;
	sqlite3* db;
	char *zErrMsg = 0;
	rc = sqlite3_open("carsDealer.db", &db);
	if (rc)
	{
		cout << "Can't open database";
		sqlite3_close(db);
		system("pause");
		return;
	}
	rc = sqlite3_exec(db, "begin;", callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return;
	}
	cout << "inital data:" << endl;
	rc = sqlite3_exec(db, "select * from cars;", callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return;
	}
	else
	{
		printTable();
		clearTable();
	}
	rc = sqlite3_exec(db, "select * from accounts;", callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return;
	}
	else
	{
		printTable();
		clearTable();
	}
	cout << "test 1:" << endl << "status: " << carPurchase(12, 15, db, zErrMsg) << endl ;
	rc = sqlite3_exec(db, "select * from cars;", callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return;
	}
	else
	{
		printTable();
		clearTable();
	}
	system("pause");
	system("cls");

	cout << "test 2:" << endl << "status: " << carPurchase(12, 1, db, zErrMsg) << endl;
	rc = sqlite3_exec(db, "select * from cars;", callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return;
	}
	else
	{
		printTable();
		clearTable();
	}
	system("pause");
	system("cls");

	cout << "test 3:" << endl << "status: " << carPurchase(11, 23, db, zErrMsg) << endl;
	rc = sqlite3_exec(db, "select * from cars;", callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return;
	}
	else
	{
		printTable();
		clearTable();
	}
	system("pause");
	system("cls");
	cout << "before transfer: " << endl;
	rc = sqlite3_exec(db, "select * from accounts;", callback, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return;
	}
	else
	{
		printTable();
		clearTable();
	}

	//----------------------------------------------------------------------
	cout << "transfer status: " << balanceTransfer(5, 12, 100000, db, zErrMsg) << endl;
	clearTable();
	cout << "after transfer: " << endl;
	rc = sqlite3_exec(db, "select * from accounts;", callback, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return;
	}
	else
	{
		printTable();
		clearTable();
	}
	system("pause");

	//----------------------------------------------------------------------
	cout <<endl << "who can buy the silver subaru(id = 17): " << endl;
	whoCanBuy(17, db, zErrMsg);
	system("pause");
	rc = sqlite3_exec(db, "rollback;", callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return;
	}

}

void whoCanBuy(int carId, sqlite3* db, char* zErrMsg)
{
	int rc;
	clearTable();
	char tab2[1024];
	string toSql = "select id from accounts GROUP BY id HAVING balance > (select price from cars WHERE id = "+ to_string(carId)+ " );" ;//group all accounts that have enough money in their balance to buy the	car
	strncpy_s(tab2, toSql.c_str(), sizeof(tab2));
	rc = sqlite3_exec(db, tab2, callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return;
	}
	else
	{
		printTable();//print the group of the accounts
		clearTable();
	}
}

bool balanceTransfer(int from, int to, int amount, sqlite3* db, char* zErrMsg)
{
	int rc;
	clearTable();
	char tab2[1024];
	string toSql = "select balance from accounts WHERE id = " + to_string(from) + " AND balance > " + to_string(amount) + ";";//asks the data base if the id has enough money
	strncpy_s(tab2, toSql.c_str(), sizeof(tab2));
	rc = sqlite3_exec(db, tab2, callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return false;
	}
	if (results.size())//if the id has enought money transfer the money between the two accounts
	{
		toSql = "UPDATE accounts SET balance = balance -" + to_string(amount) + " WHERE id = " + to_string(from) + ";";
		strncpy_s(tab2, toSql.c_str(), sizeof(tab2));
		rc = sqlite3_exec(db, tab2, callback, 0, &zErrMsg);
		if (rc != SQLITE_OK)
		{
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			return false;
		}
		toSql = "UPDATE accounts SET balance = balance + " + to_string(amount) + " WHERE id = " + to_string(to) + ";";
		strncpy_s(tab2, toSql.c_str(), sizeof(tab2));
		rc = sqlite3_exec(db, tab2, callback, 0, &zErrMsg);
		if (rc != SQLITE_OK)
		{
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			return false;
		}
		return true;
	}
	return false;
}

bool carPurchase(int buyerid, int carid, sqlite3* db, char* zErrMsg)
{
	int rc;
	clearTable();
	char tab2[1024];
	string toSql = "select * from cars WHERE id =" + to_string(carid) + " AND available = 1;";
	strncpy_s(tab2, toSql.c_str(), sizeof(tab2));
	rc = sqlite3_exec(db, tab2, callback, 0, &zErrMsg);//checks if the car is available(available = 1)
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return false;
	}
	if (results.size())//if the car is available
	{
		clearTable();
		toSql = "select price from cars WHERE id = " + to_string(carid) + " and price < (select balance from accounts WHERE id = " + to_string(buyerid) + ");";//checks if the buyer has enough money(if he has it returns something)
		strncpy_s(tab2, toSql.c_str(), sizeof(tab2));

		rc = sqlite3_exec(db, tab2, callback, 0, &zErrMsg);
		if (rc != SQLITE_OK)
		{
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			return false;
		}
	}
	if (results.size())//if he has enough money
	{
		clearTable();
		toSql = "UPDATE accounts SET balance = balance - (select price from cars WHERE id = " + to_string(carid) + ") WHERE id = " + to_string(buyerid) + ";";//update the balance of the buyer's account
		strncpy_s(tab2, toSql.c_str(), sizeof(tab2));
		rc = sqlite3_exec(db, tab2, callback, 0, &zErrMsg);
		if (rc != SQLITE_OK)
		{
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			return false;
		}
		toSql = "UPDATE cars SET available = 0 WHERE id = " + to_string(carid) + ";";//set the available(of the car) to 0
		strncpy_s(tab2, toSql.c_str(), sizeof(tab2));
		rc = sqlite3_exec(db, tab2, callback, 0, &zErrMsg);
		if (rc != SQLITE_OK)
		{
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			return false;
		}
		return true;
	}
	return false;
}


int callback(void* notUsed, int argc, char** argv, char** azCol)
{
	int i;

	for (i = 0; i < argc; i++)
	{
		auto it = results.find(azCol[i]);
		if (it != results.end())
		{
			it->second.push_back(argv[i]);
		}
		else
		{
			pair<string, vector<string>> p;
			p.first = azCol[i];
			p.second.push_back(argv[i]);
			results.insert(p);
		}
	}

	return 0;
}

void clearTable()
{
	for (auto it = results.begin(); it != results.end(); ++it)
	{
		it->second.clear();
	}
	results.clear();
}

void printTable()
{
	auto iter = results.end();
	iter--;
	//	int size = results.begin()->second.size();
	int size = iter->second.size();
	for (int i = -2; i < size; i++)
	{
		for (auto it = results.begin(); it != results.end(); ++it)
		{
			if (i == -2)
			{
				//cout << it->first << " ";
				printf("|%*s|", 13, it->first.c_str());
			}
			else if (i == -1)
				cout << "_______________";
			else
				//	cout << it->second.at(i) << " ";
				printf("|%*s|", 13, it->second.at(i).c_str());
		}
		cout << endl;
	}
}